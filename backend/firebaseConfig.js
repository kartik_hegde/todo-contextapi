const firebase = require("firebase-admin");
const { getFirestore } = require("firebase-admin/firestore");

var serviceAccount = require("./keys.json");

firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount),
});

const db = getFirestore();
const Users = db.collection("users").doc();

module.exports = Users;
