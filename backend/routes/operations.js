const express = require("express");
const router = express.Router();
const {
  getFirestore,
} = require("firebase-admin/firestore");

const db = getFirestore();
// ROUTE 1: Get All the todos using:
router.get("/fetchall", async (req, res) => {

  try {
    const todosRef = db.collection("todos");
    const snapshot = await todosRef.get();
    const documents = [];
    snapshot.forEach((doc) => { 
      let senddata=doc.data()
      senddata.id=doc.id
      documents.push(senddata);
    });
  
    res.json(documents);
  } catch (error) {
    console.error(error.message);
    res.status(500).send("Internal Server Error");
  }
});

// ROUTE 2: Add a new car using: POST "
router.post("/add", async (req, res) => {
  try {
    const {
      id,
      todo,
      completed
    } = req.body;

    const Todos = db.collection("todos");

    const todos = await Todos.add({
      id:id,
      todo:todo,
      completed:completed
    });
    console.log("this is the id " + todos.id);

    res.json({ success: "new todo successfully added to database" });
  } catch (error) {
    res.status(500).send({ error: error.message });
  }
});

//update todos
router.post("/updateTodo", async (req, res) => {
    try {
      const { todo , id, check } = req.body;
      const snapshot = await db
        .collection("todos")
        .where("id", "==", id)
        .get(1);
  
      if (snapshot._size > 0) {
        if (check){
            snapshot.forEach((doc) => {
                db.collection("todos")
                  .doc(doc.id)
                  .update({completed : !doc.data().completed})
                  .then(() => {
                    
                    console.log("Document successfully updated!");
                  })
                  .catch((error) => {
                    console.error("Error updating document: ", error);
                  });
              });
        
        
        }else{
            snapshot.forEach((doc) => {
                db.collection("todos")
                  .doc(doc.id)
                  .update({todo : todo})
                  .then(() => {
                    console.log("Document successfully updated!");
                  })
                  .catch((error) => {
                    console.error("Error updating document: ", error);
                  });
              });
        }
      }
  
      if (!snapshot) {
        return res.status(404).send("Not Found");
      }
  
      res.json({ success: "todo has been updated" });
    } catch (error) {
      console.error(error.message);
      res.status(500).send("Internal Server Error");
    }
  });
  


//  delete an existing todo
router.post("/deleteTodo", async (req, res) => {
  try {
    const { id } = req.body;
    const snapshot = await db
      .collection("todos")
      .where("id", "==", id)
      .get(1);

    if (snapshot._size > 0) {
      snapshot.forEach((doc) => {
        db.collection("todos")
          .doc(doc.id)
          .delete()
          .then(() => {
            console.log("Document successfully deleted!");
          })
          .catch((error) => {
            console.error("Error deleting document: ", error);
          });
      });

      return res.json({ success: "successfully deleted" });
    }

    if (!snapshot) {
      return res.status(404).send("Not Found");
    }

    res.json({ success: "todo has been deleted" });
  } catch (error) {
    console.error(error.message);
    res.status(500).send("Internal Server Error");
  }
});

module.exports = router;