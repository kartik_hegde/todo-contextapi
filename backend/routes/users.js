const express = require("express");
const router = express.Router();
const bcrypt = require("bcrypt");
const Users = require("../firebaseConfig");
const { body, validationResult } = require("express-validator");
const { getFirestore } = require("firebase-admin/firestore");

const db = getFirestore();

router.post(
  "/createuser",
  [
    body("name", "Enter a valid name").isLength({ min: 3 }),
    body("email", "enter a valid").isEmail(),
    body("password", "atleast 3 char").isLength({ min: 3 }),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      const { email, password, name } = req.body;
      let new_pass;
      let new_id;

      const snapshot = await db
        .collection("users")
        .where("email", "==", email)
        .get(1);

      if (snapshot.empty) {
        const salt = 10;

        const encrypt = await bcrypt.hash(password, salt);

        const Users = db.collection("users");
        const user = await Users.add({
          name: name,
          email: email,
          password: encrypt,
        });

        res.json({ user });
      } else {
        let user;
        snapshot.forEach((doc) => {
          (user = doc.data().name), (new_pass = doc.data().password);
          new_id = doc.data().email;
        });
        const compare = bcrypt.compare(password, new_pass);
        if (!compare) {
          return res.status(400).json({ error: "Please try to login again" });
        }

        res.json({ user });
      }
    } catch (error) {
      res.status(500).send({ msg: error.message });
    }
  }
);

module.exports = router;
