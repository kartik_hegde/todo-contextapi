import { useState ,useContext } from "react";
import Context from "./Context";
import { Typography, Button, Flex } from "antd";
import Forms from "./components/Forms";
import Todos from "./components/Todos";

function App() {
  const { Title } = Typography;
  
  let { todos ,deleteAll}= useContext(Context)
  

  //edit form ...
  const [editFormVisibility, setEditFormVisibility] = useState(false);
  const [editTodo, setEditTodo] = useState("");

  const handleEditClick = (todo) => {
    setEditFormVisibility(true);
    setEditTodo(todo);
  };

  // back button
  const cancelUpdate = () => {
    setEditFormVisibility(false);
  };

  return (
    <div>
      <Title
        style={{
          color: "black",
          fontSize: "50px",
          textDecoration: "underline",
        }}
      >
        TODO-APP USING REACT-REDUX
      </Title>
      <Forms
        editFormVisibility={editFormVisibility}
        editTodo={editTodo}
        cancelUpdate={cancelUpdate}
      />
      <Flex wrap="wrap" gap="small">
        <Todos
          handleEditClick={handleEditClick}
          editFormVisibility={editFormVisibility}
        />
      </Flex>

      {todos.length > 1 ? (
        <Button
          className="wrapper" 
          type="primary"
          danger
          onClick={() =>
            deleteAll()
          }
        >
          {" "}
          DELETE ALL
        </Button>
      ) : null}
    </div>
  );
}

export default App;
