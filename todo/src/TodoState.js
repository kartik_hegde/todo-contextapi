import React, { useState } from "react";
import Context from "./Context";

const TodoState = (props) => {
  const [todos, settodos] = useState([]);

  const addTodo = (payload) => {
    settodos([...todos, payload]);
    return;
  };

  const deleteAll = () => {
    settodos([]);
    return;
  };

  const removeTodo = (payload) => {
    const filteredTodos = todos.filter((todo) => todo.id !== payload.id);
    settodos(filteredTodos);
    return;
  };

  const handleUpdate = (check, payload) => {
    let updatedArray = [];

    if (check) {
      todos.forEach((item) => {
        if (item.id === payload.id) {
          item.completed = !item.completed;
        }
        updatedArray.push(item);
      });
    } else {
      todos.forEach((item) => {
        if (item.id === payload.id) {
          item.id = payload.id;
          item.todo = payload.todo;
          item.completed = payload.completed;
        }
        updatedArray.push(item);
      });
    }
    settodos(updatedArray);
    return;
  };

  return (
    <Context.Provider
      value={{
        todos,
        handleUpdate,
        deleteAll,
        addTodo,
        removeTodo,
      }}
    >
      {props.children}
    </Context.Provider>
  );
};

export default TodoState;
