import React, { useState, useRef, useContext,useEffect } from "react";
import Context from "../Context";
import { Button, Input, Switch, Form } from "antd";

const Forms = (props) => {
  let {addTodo ,handleUpdate } =useContext(Context)
  const inputRef = useRef(null);
  let { editFormVisibility, editTodo, cancelUpdate } = props;


  const [todoValue, setTodoValue] = useState("");

  const [editValue, setEditValue] = useState("");

  useEffect(() => {
    setEditValue(editTodo.todo);
  }, [editTodo]);

  
  const [input, setInput] = useState(true);

  const handleSubmit = () => {
    
    const date = new Date();
    const time = date.getTime();
    const todoObj = {
      id: time,
      todo: todoValue,
      completed: false,
    }; 
    addTodo(todoObj)
    setTodoValue("")
  };

  const handlefail = () => {
    return
  };

  const editSubmit = () => {
    const editedObj = {
      id: editTodo.id,
      todo: editValue,
      completed: false,
    };
    handleUpdate(false,editedObj)

  };
  const sharedProps = {
    style: {
      width: "100%",
    },
    defaultValue: "",
    ref: inputRef,
  };

  return (
    <>
      {editFormVisibility === false ? (
        <Form
          className="wrapper"
          name="add-todo"
          autoComplete="off"
          onFinish={handleSubmit}
          onFinishFailed={handlefail}
        >
          <Switch
            className="switch"
            checked={input}
            checkedChildren="Input"
            unCheckedChildren="TextArea"
            onChange={() => {
              setInput(!input);
            }}
          />

          {input ? (
            <Form.Item
              label=""
              name="todo"
              rules={[
                {
                  required: true,
                  message: "Please input your task!",
                },
              ]}
            >
              <Input
                value={todoValue}
                onChange={(e) => setTodoValue(e.target.value)}
                {...sharedProps}
              />
            </Form.Item>
          ) : (
            <Form.Item
              label=""
              name="txtarea"
              rules={[
                {
                  required: true,
                  message: "Please input your task!",
                },
              ]}
            >
              <Input.TextArea
                value={todoValue}
                onChange={(e) => setTodoValue(e.target.value)}
                {...sharedProps}
              />
            </Form.Item>
          )}

          <Form.Item>
            <Button type="primary" htmlType="submit">
              {" "}
              ADD
            </Button>
          </Form.Item>
        </Form>
      ) : (
        <Form
          name="edit-todo"
          autoComplete="off"
          onFinish={editSubmit}
          onFinishFailed={handlefail}
        >
          {" "}
          <Form.Item
            label=""
            name="editinp"
            rules={[
              {
                required: true,
                message: "Please input your task!",
              },
            ]}
          >
            <Input
              value={editValue || ""}
              onChange={(e) => setEditValue(e.target.value)}
            />
          </Form.Item>
          <Form.Item style={{ float: "right", margin: "10px" }}>
            <Button type="primary"  htmlType="submit">
              UPDATE
            </Button>
          </Form.Item>
          <Form.Item style={{ float: "right", margin: "10px" }}>
            <Button type="primary" onClick={cancelUpdate}>
              BACK
            </Button>
          </Form.Item>
        </Form>
      )}
    </>
  );
};

export default Forms;
