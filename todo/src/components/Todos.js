import React,{useContext} from "react";
import { DeleteFilled, EditFilled } from "@ant-design/icons";
import { Checkbox , Space } from "antd";
import Context from "../Context";
const Todos = (props) => {
  const { handleEditClick, editFormVisibility } = props;
  let { todos,handleUpdate ,removeTodo} =useContext(Context)


  return todos.map((todo) => (
    <div key={todo.id} className="todo-box">
      <div className="content">
        {editFormVisibility === false && (
          <Checkbox
            checked={todo.completed}
            onChange={() => {
              handleUpdate(true,todo)
            }}
          ></Checkbox>  
        )}
        <div className="para">
          <p
            style={
              todo.completed === true
                ? { textDecoration: "line-through" }
                : { textDecoration: "none" }
            }
          >
            {todo.todo}
          </p>
        </div>
      </div>
      <div className="actions-box">
        {editFormVisibility === false && (
          <>
            <Space className="icons" onClick={() => handleEditClick(todo)}>
              <EditFilled />
            </Space>
            <Space
              className="icons"
              onClick={() => removeTodo(todo)}
            >
              
              <DeleteFilled />
            </Space>
          </>
        )}
      </div>
    </div>
  ));
};
export default Todos;
